# Duffel UI Challenge

## Goal

Using the provided dataset and mockup, we'd like you to create a simple single-page application using plain JavaScript (no libraries), HTML and CSS.

## Mockup

![mockup](https://github.com/duffelhq/frontend-challenge/blob/master/support/mockup.png?raw=true)

## Dev system requirements:

You must have the following intalled in your machine:

-   [node](https://nodejs.org/en/)
-   [npm](https://www.npmjs.com/get-npm) or [yarn](https://yarnpkg.com/lang/en/docs/install)

This project was built using:

| Program | Version |
| ------- | ------- |
| node    | 10.6.1  |
| yarn    | 1.10.1  |
| vs-code | 1.30.1  |

For packages information please refer to `package.json`

## Get started

### Instalation

```sh
npm i
# or #
yarn
```

### Run in development

```sh
npm run dev
# or #
yarn dev
```

### Run Tests

```sh
npm run test
# or #
yarn test
```

### Run Build

```sh
npm run build
# or #
yarn build
```

Site will be avaiable on `./dist`
