import DogNames from '../src/assets/data/names.json'
import * as Repo from '../src/repository'

describe('DATA SET', () => {
	// it('should have consistent structure', () => {
	// 	DogNames.forEach(element => {
	// 		// CHECK KEYS
	// 		expect(element).toHaveProperty('name')
	// 		expect(element).toHaveProperty('gender')
	// 		expect(element).toHaveProperty('breed')
	// 		expect(element).toHaveProperty('borough')
	// 		// CHECK GENDER
	// 		const { gender } = element
	// 		expect(['MALE', 'FEMALE']).toContain(gender)
	// 	})
	// })
})

describe('REPOSITORY', () => {
	it('should give the right dataset size', () => {
		const SIZE = Repo.getDataSetLength(DogNames)
		expect(SIZE).toBe(DogNames.length)
		expect(SIZE).toBe(DogNames.length)
	})

	it('should not mutate orginal dataset on fetch', () => {
		const first10 = Repo.fetchData(DogNames, 10)
		expect(first10.length).toBe(10)

		const SIZE = Repo.getDataSetLength(DogNames)
		expect(SIZE).toBe(DogNames.length)
	})

	it('should build proper insight object', () => {
		const L = 5000
		const insight = Repo.getDataInsight(
			DogNames,
			Repo.fetchData(DogNames, L)
		)

		for (const dogName of Object.keys(insight)) {
			const dogNameData = insight[dogName]

			expect(dogNameData).toHaveProperty('count')
			expect(dogNameData.count).toHaveProperty('MALE')
			expect(dogNameData.count).toHaveProperty('FEMALE')

			expect(dogNameData.breeds).toBeInstanceOf(Object)

			const totalcount = dogNameData.count.MALE + dogNameData.count.FEMALE
			const totalBreeds = Object.keys(dogNameData.breeds)
				.map(breed => dogNameData.breeds[breed])
				.reduce((prev, curr) => prev + curr)

			expect(totalcount).toEqual(totalBreeds)
		}
	})

	it('should build proper ranking array', () => {
		const L = 5000
		const data = Repo.fetchData(DogNames, L)

		const insight = Repo.getDataInsight(data)
		const ranking = Repo.getRankingFromInsight(insight)

		for (const i in ranking) {
			if (i !== '0') {
				// check overall ranking
				expect(ranking[i - 1].totalCount).toBeGreaterThanOrEqual(
					ranking[i].totalCount
				)
			}

			for (const j in ranking[i].breeds) {
				if (j !== '0') {
					// check breed ranking
					expect(
						ranking[i].breeds[j - 1].count
					).toBeGreaterThanOrEqual(ranking[i].breeds[j].count)
				}
			}

			// check breeds add up
			const breedSum = ranking[i].breeds
				.map(({ count }) => count)
				.reduce((prev, curr) => prev + curr)
			expect(breedSum).toBe(ranking[i].totalCount)
		}
	})

	it('should filter ranking data by breed', () => {
		const data = Repo.fetchData(DogNames)

		const insight = Repo.getDataInsight(data)
		const ranking = Repo.getRankingFromInsight(insight)

		const TEST_BREED = 'Yorkshire Terrier'

		const filteredData = Repo.filterRankingData(ranking, {
			breed: TEST_BREED
		})

		for (const i in filteredData) {
			filteredData.map(({ breeds }) => {
				breeds.map(({ breed }) => expect(breed).toBe(TEST_BREED))
			})

			if (i !== '0') {
				// check overall ranking
				const prevCount = filteredData[i - 1].breeds.filter(
					({ breed }) => breed === TEST_BREED
				)[0]
				const currCount = filteredData[i].breeds.filter(
					({ breed }) => breed === TEST_BREED
				)[0]

				expect(prevCount).toBeGreaterThanOrEqual(currCount)
			}
		}
	})
})
