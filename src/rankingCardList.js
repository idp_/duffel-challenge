import MissingDogSvg from './assets/images/missing-dog.svg'
import { filterRankingData } from './repository'

let _rankingData = null
let _nameFilter = null
let _breedFilter = null

// EVENT HANNDLERS
let _breedClickEvent = null

let rankingCardListContainerID

export default class RankingCardList {
	constructor(RankingCardListContainerID, eventHandlers) {
		rankingCardListContainerID = RankingCardListContainerID
		this.eventHandlers = eventHandlers
	}

	get ranking() {
		return filterRankingData(_rankingData, {
			name: _nameFilter,
			breed: _breedFilter
		})
	}

	set ranking(newRanking) {
		_rankingData = newRanking
		this.onDataUpdated()
	}

	get nameFilter() {
		return _nameFilter
	}

	set nameFilter(newfilter) {
		_nameFilter = newfilter || null
		this.onDataUpdated()
	}

	set breedClickEvent(fn) {
		_breedClickEvent = fn
		this.onDataUpdated()
	}

	get breedClickEvent() {
		return _breedClickEvent
	}

	get breedFilter() {
		return _breedFilter
	}

	set breedFilter(newfilter) {
		_breedFilter = newfilter || null
		this.onDataUpdated()
	}

	onDataUpdated() {
		this.render()
	}

	render() {
		setImmediate(() => renderRanking(this.ranking, this.eventHandlers))
	}
}

// RENDER HELPERS
// ======================================
/**
 * Renders the ranking array into the list
 *
 * @param {{ name: string, totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed:string, count:number }[]  }[]} rankingArr The array of dog name rankings
 * @param { { eventName: Function } } eventHandlers Add event handlers
 * @return {undefined}
 */
function renderRanking(rankingArr, eventHandlers) {
	clearList()

	if (rankingArr.length === 0) renderInList(emptyView())
	rankingArr
		.map(dogNameStats =>
			buildRankingCard(
				dogNameStats.name,
				dogNameStats.count,
				dogNameStats.breeds,
				dogNameStats.ranking,
				eventHandlers
			)
		)
		.forEach(element => renderInList(element))
}

/**
 * Removes all existing elements from the list container
 *
 * @returns {undefined}
 */
function clearList() {
	const list = document.querySelector(`#${rankingCardListContainerID}`)
	while (list.firstChild) list.removeChild(list.firstChild)
}

/**
 * Renders an element onto the list conntainer
 *
 * @param {HTMLElement} element The ranking card to be rendered on the list
 * @returns {undefined}
 */
function renderInList(element) {
	const list = document.querySelector(`#${rankingCardListContainerID}`)
	list.appendChild(element)
}

// ELEMENNT BUILDER : EMPTY VIEW
// ======================================
/**
 * Builds the empty view for no search matches
 *
 * @returns {HtmlDivElement} The Empty View element
 */
function emptyView() {
	const emptyEl = document.createElement('div')
	emptyEl.classList.add('empty-view')

	const missingDogImg = new Image()
	missingDogImg.src = MissingDogSvg

	const textEl = document.createElement('div')
	textEl.innerHTML = `No dog names match '<b>${_nameFilter}</b>'`

	emptyEl.appendChild(missingDogImg)
	emptyEl.appendChild(textEl)

	return emptyEl
}

// ELEMENNT BUILDER : CARD CONTAINER
// ======================================
/**
 * Builds the Ranking card for a given dog name
 *
 * @param {string} name The dog name
 * @param {{MALE: number, FEMALE:number}} count The gender distribution count object
 * @param {{breed: string, count: number}[]} topBreeds The top breeds array
 * @param {number} ranking The ranking position for the given name
 * @param { { eventName: Function } } eventHandlers Add event handlers
 *
 * @returns {HtmlDivElement} The ranking card element to be rendered onto the list
 */
function buildRankingCard(name, count, topBreeds, ranking, eventHandlers) {
	const cardEl = document.createElement('div')
	cardEl.classList.add('ranking-card')

	cardEl.appendChild(buildRankingCardHeader(name, count, ranking))
	cardEl.appendChild(document.createElement('hr'))
	cardEl.appendChild(buildBreedDistribution(topBreeds, eventHandlers))

	return cardEl
}

// ELEMENNT BUILDER : CARD HEADER
// ======================================
/**
 * Builds the Ranking card header element
 *
 * @param {string} name The dog name
 * @param {{MALE: number, FEMALE:number}} count The gender distribution count object
 * @param {number} ranking The ranking position for the given name
 *
 * @returns {HtmlDivElement} The ranking card header element to be appended to ranking card
 */
function buildRankingCardHeader(name, count, ranking) {
	const headerEl = document.createElement('div')
	headerEl.classList.add('ranking-card-header')

	const leftSide = document.createElement('div')
	leftSide.appendChild(buildRankingTile(ranking))
	leftSide.appendChild(buildCardTitle(name, count))
	headerEl.appendChild(leftSide)

	const rightSide = buildGenderDistribution(count)
	headerEl.appendChild(rightSide)

	return headerEl
}

/**
 * Builds the ranking tile for the card header
 *
 * @param {number} ranking The ranking position for the given name
 *
 * @returns {HtmlDivElement} The ranking tile element to be appended to ranking card header
 */
function buildRankingTile(ranking) {
	const rankingTile = document.createElement('div')
	rankingTile.classList.add('ranking-tile')
	rankingTile.innerText = ranking
	return rankingTile
}

/**
 * Builds the ranking title with name and dog count
 *
 * @param {string} name The dog name
 * @param {{MALE: number, FEMALE:number}} count The gender distribution count object
 *
 * @returns {HtmlDivElement} The card title element to be appended to ranking card header
 */
function buildCardTitle(name, count) {
	const titleEl = document.createElement('div')
	titleEl.classList.add('card-title')

	const nameEl = document.createElement('div')
	nameEl.innerText = name

	const countEl = document.createElement('span')
	const totalCount = count.MALE + count.FEMALE
	countEl.innerText = `${totalCount} dogs`

	titleEl.appendChild(nameEl)
	titleEl.appendChild(countEl)

	return titleEl
}

/**
 * Builds the gender distribution header element
 *
 * @param {{MALE: number, FEMALE:number}} count The gender distribution count object
 *
 * @returns {HtmlDivElement} The card gender distribution element to be appended to ranking card header
 */
function buildGenderDistribution(count) {
	const genderDistributionEl = document.createElement('div')
	genderDistributionEl.classList.add('gender-distribution')

	const total = count.MALE + count.FEMALE

	genderDistributionEl.appendChild(
		buildPercentageStat('Male', count.MALE / total)
	)
	genderDistributionEl.appendChild(
		buildPercentageStat('Female', count.FEMALE / total)
	)

	return genderDistributionEl
}

/**
 * Builds the gender Percentile display with label above
 *
 * @param {string} label The label to be displayed
 * @param {number} percentage Percentile to be displayed `0 - 1`
 *
 * @returns {HtmlDivElement} The percentage stat element to be appended to gender distribution conntainer
 */
function buildPercentageStat(label, percentage) {
	const percentageEl = document.createElement('div')
	percentageEl.classList.add('percentage-stat')

	const labelEl = document.createElement('div')
	labelEl.innerText = label
	percentageEl.appendChild(labelEl)

	const percentEl = document.createElement('span')
	percentEl.innerText = `${Math.round(percentage * 100)}%`
	percentageEl.appendChild(percentEl)

	return percentageEl
}

// ELEMENNT BUILDER : BREEDS HISTOGRAM
// ======================================
/**
 * Builds breed ranking histogram element
 *
 * @param {{breed: string, count: number}[]} topBreeds The top breeds array
 * @param { { eventName: Function } } eventHandlers Add event handlers
 * @param {boolean?} shouldRenderUnknown Should render unknown breed
 *
 * @returns {HtmlDivElement} The titled histogram with top 4 breeds
 */
function buildBreedDistribution(
	topBreeds,
	eventHandlers,
	shouldRenderUnknown = false
) {
	const breedsEl = document.createElement('div')
	breedsEl.classList.add('breeds-container')

	const titleEl = document.createElement('div')
	titleEl.classList.add('title')
	titleEl.innerHTML = 'Top Breeds'
	breedsEl.appendChild(titleEl)

	const breedsToRender = _breedFilter
		? topBreeds.filter(({ breed }) => breed === _breedFilter)
		: topBreeds.slice(0, 4)

	if (breedsToRender.some(({ breed }) => breed === 'null')) {
		const indx = breedsToRender.findIndex(({ breed }) => breed === 'null')
		if (!shouldRenderUnknown) {
			breedsToRender.splice(indx, 1)
			breedsToRender.push(topBreeds[5])
		} else breedsToRender[indx].breed = 'Unknown'
	}

	const gridEl = buildBreedGrid(breedsToRender, eventHandlers)
	breedsEl.appendChild(gridEl)

	return breedsEl
}

/**
 * Builds the histogram element
 *
 * @param {{breed: string, count: number}[]} topBreeds The top breeds array
 * @param { { eventName: Function } } eventHandlers Add event handlers
 *
 * @returns {HtmlDivElement} The histogram element
 */
function buildBreedGrid(topBreeds, eventHandlers) {
	const gridEl = document.createElement('div')
	gridEl.classList.add('breeds-grid')

	const topDogCount = topBreeds[0].count
	topBreeds.forEach(({ breed, count }) => {
		const row = renderGridRow(
			breed,
			count,
			count / topDogCount,
			eventHandlers
		)
		gridEl.appendChild(row[0])
		gridEl.appendChild(row[1])
		gridEl.appendChild(row[2])
	})

	return gridEl
}

/**
 * Builds a grid row for the histogram
 *
 * @param {string} breed The breed label to be displayed onn histogram row
 * @param {number} count The number to be displayed on histogram row
 * @param {number} percentage The histogram percentage to be displayed on histogram row
 * @param { { eventName: Function } } eventHandlers Add event handlers
 *
 * @returns {HtmlDivElement[]} A grid row for the histogram `[0] breed` `[1] hist bar` `[2] hist number`
 */
function renderGridRow(breed, count, percentage, eventHandlers) {
	const gridRowEl = []

	const nameEl = document.createElement('div')
	const nameLink = document.createElement('a')
	nameLink.classList.add('clickable')
	nameLink.innerText = breed

	if (eventHandlers && eventHandlers.breedClick)
		nameLink.addEventListener('click', eventHandlers.breedClick)

	nameEl.appendChild(nameLink)
	gridRowEl.push(nameEl)

	const barContaienrEl = document.createElement('div')
	const barEl = document.createElement('div')
	barEl.classList.add('display-bar')
	barEl.style.width = `${percentage * 100}%`

	barContaienrEl.appendChild(barEl)
	barContaienrEl.classList.add('bar-container')
	barContaienrEl.style.width = '100%'
	gridRowEl.push(barContaienrEl)

	const countEl = document.createElement('div')
	countEl.innerText = count
	gridRowEl.push(countEl)

	return gridRowEl
}
