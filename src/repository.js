/**
 * Fetches raw data set and builds teh array of rankings
 *
 * @param {number} topN The number of top rankings you want to retrieve
 * @param {Function} onCompletionCallback It is called when the array of `topN` rankings is ready
 *
 * @returns {undefined} Ranking data is passed to `onCompletionCallback`
 */
export function getRankingFromDataSet(topN, onCompletionCallback) {
	// CHECK RANKING DATA
	if (RANKINGS.num >= topN)
		onCompletionCallback(RANKINGS.array.slice(0, topN))

	// CHECK LOCAL STORAGE
	const localData = JSON.parse(localStorage.getItem('dogRanking'))
	if (localData) {
		RANKINGS.num = localData.length
		RANKINGS.array = localData
		if (RANKINGS.num >= topN)
			onCompletionCallback(RANKINGS.array.slice(0, topN))
	}

	// COMPUTE DATA
	setImmediate(() => {
		// LOAD RAW DATA
		const DogData = require('./assets/data/names.json')
		const data = fetchData(DogData, getDataSetLength(DogData))
		// BUILD INSIGHT OBJECT
		const insight = getDataInsight(data)
		// RANK INSIGHT
		const ranking = getRankingFromInsight(insight).slice(0, topN)
		// COMPLETION CALLBACK
		onCompletionCallback(ranking)
		// STORE DATA FOR FUTURE USE
		RANKINGS.num = topN
		RANKINGS.array = ranking
		// PERSIST ON LOCAL STORAGE
		localStorage.setItem('dogRanking', JSON.stringify(ranking))
	})
}
const RANKINGS = {}

/**
 * Returns the length of the data set
 * @param { Array } DogData The data array to retrieve length
 * @return {number} The total length of the data set
 */
export function getDataSetLength(DogData) {
	if (!DATA_LENGTH) DATA_LENGTH = DogData.length
	return DATA_LENGTH
}
let DATA_LENGTH

/**
 * Fetches a segment of the dataset
 *
 * @param {Array} DogData The Raw Dog data
 * @param {number} amount The amount you want to retrieve from the dataset
 * @param {number} offset Where do you want to start fetching
 * @return {Array} The data segment you requested
 */
export function fetchData(DogData, amount, offset = 0) {
	return DogData.slice(offset, offset + amount)
}

/**
 * Gets the data insight object from the raw dataset
 *
 * @param {{ name: string, breed: string, gender: string, borough: string  }} rawDataSet the dataset to be processed
 * @returns {{ dogName: { totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed : number } } }} The data insight object
 */
export function getDataInsight(rawDataSet) {
	const INSIGHT = {}
	rawDataSet.forEach(({ name, gender, breed }) => {
		name = name.toLowerCase()
		if (!(name in INSIGHT)) {
			INSIGHT[name] = {
				count: { MALE: 0, FEMALE: 0 },
				breeds: {}
			}
		}

		INSIGHT[name].count[gender] += 1

		if (!INSIGHT[name].breeds[breed]) INSIGHT[name].breeds[breed] = 0
		INSIGHT[name].breeds[breed] += 1
	})

	return INSIGHT
}

/**
 * Builds a ranking array based on a given `insight` object
 *
 * @param {{ dogName: { totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed : number } } }} insight The insight extracted from raw dataset
 * @returns { { name: string, totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed:string, count:number }[]  }[] } The array of rankings
 */
export function getRankingFromInsight(insight) {
	return Object.keys(insight)
		.map(name => ({
			name,
			count: insight[name].count,
			totalCount: addCountForObject(insight[name].count),
			breeds: rankBreeds(insight[name].breeds)
		}))
		.sort((a, b) => b.totalCount - a.totalCount)
		.map((el, i) => {
			el.ranking = i + 1
			return el
		})
}

/**
 * Builds the sorted ranking array of breeds
 *
 * @param {{ breed: number }} breeds The breeds to be sorted
 * @returns {{ breed: string, count:number }[]} The breeds ranking array
 */
export function rankBreeds(breeds) {
	const breedsArray = Object.keys(breeds)
		.map(breed => ({
			breed,
			count: breeds[breed]
		}))
		.sort((a, b) => b.count - a.count)

	return breedsArray
}

/**
 * Adds all number values for a given object
 *
 * @param {{ key:number... }} count The object with count values
 * @returns {number} The sum of male and female objects
 */
function addCountForObject(count) {
	return Object.keys(count)
		.map(key => count[key])
		.reduce((prev, curr) => prev + curr)
}

/**
 * Filters ranking object
 *
 * @param { { name: string, totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed:string, count:number }[]  }[] } rankingData The complete ranking object
 * @param { { name:string, breed:string } } filterOptions Defie how to filter/sort ranking
 * @returns { { name: string, totalCount: number, count: { MALE: number, FEMALE: number }, breeds: { breed:string, count:number }[]  }[] } The filtered rannkinng obj
 */
export function filterRankingData(rankingData, filterOptions) {
	const nameFilter = filterOptions.name
	const breedFilter = filterOptions.breed

	if (nameFilter) {
		rankingData = rankingData.filter(({ name }) =>
			name.toLowerCase().includes(nameFilter.toLowerCase())
		)
	}

	if (breedFilter) {
		rankingData = rankingData
			.filter(({ breeds }) =>
				breeds.some(({ breed }) => breed === breedFilter)
			)
			.sort((a, b) => {
				const prevFilterBreed = a.breeds.filter(
					({ breed }) => breed === breedFilter
				)[0]
				const currFilterBreed = b.breeds.filter(
					({ breed }) => breed === breedFilter
				)[0]
				return currFilterBreed.count - prevFilterBreed.count
			})
	}

	return rankingData
}
