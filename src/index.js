import './assets/styles/styles.css'
import { getRankingFromDataSet } from './repository'
import RankingCardList from './rankingCardList'

const rankingCardList = new RankingCardList('list-container', {
	breedClick
})

/**
 * APP INIT FUNCTION
 *
 * @returns {undefined}
 */
function init() {
	loadRankings()
	assignSearchInputHandler()
	setupCloseFilterFn()
}

/**
 * Loads up rankings and assigns it to RankingCardList component
 *
 * @returns {undefined}
 */
function loadRankings() {
	getRankingFromDataSet(500, result => {
		rankingCardList.ranking = result
	})
}

/**
 * Sets up the onchange handler for the search input
 *
 * @returns {undefined}
 */
function assignSearchInputHandler() {
	const searchInputEl = document.querySelector('#search input')
	searchInputEl.addEventListener('input', handleSearchInputChange)
}

function displayCloseFilterBtn() {
	const removeFilterBtn = document.querySelector('#remove-filters')
	removeFilterBtn.classList.remove('hidden')
}

function breedClick(e) {
	rankingCardList.breedFilter = e.target.innerText
	displayCloseFilterBtn()
}

function setupCloseFilterFn() {
	const removeFilterBtn = document.querySelector('#remove-filters')
	removeFilterBtn.addEventListener('click', e => {
		rankingCardList.breedFilter = null
		rankingCardList.nameFilter = null
		e.target.classList.add('hidden')
	})
}

/**
 * Assigns
 *
 * @param { KeyboardEvent<HTMLInputElement> } event The native keyboard event to be handled
 * @returns {undefined}
 */
function handleSearchInputChange(event) {
	const targetValue = event.target.value
	rankingCardList.nameFilter = targetValue.trim()
	displayCloseFilterBtn()
}

// ⚡️ INIT
window.addEventListener('load', init)
